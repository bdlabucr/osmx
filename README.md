# osm-extractor for Beast project
## Intro
Parse OpenStreetMap .pbf file and generate datasets with geometries in WKT format in .csv files or GeoJSON, as well as corresponding satellite images of the geometries.

## Prerequisites
* JAVA 1.8
* MAVEN
* SPARK 3.0.0 Pre-build for Apache Hadoop 2.7
* [Beast](https://bitbucket.org/bdlabucr/beast/wiki/Home)

## Quick Start
1. Command line interface (CLI)
The following part download, compiles, and runs the code.
It assumes that Beast is downloaded and configured.
   Check [this link](https://bitbucket.org/bdlabucr/beast/wiki/Home) for how to install Beast.
```shell
# Grab the latest code
git clone https://eldawy@bitbucket.org/bdlabucr/osmx.git
cd ./osmx
mvn package
# Download a sample PBF file
wget https://download.geofabrik.de/antarctica-latest.osm.pbf
# Compile and run the extractor
./run.sh -p antarctica-latest.osm.pbf [options...]

USAGE: ./run.sh [options...]
 -d, --dataset        Dataset: all_nodes, pois, roads, road_network, all_objects
                      buildings, lakes, parks, cemetery, sports, postal_codes
     --dataformat     Output format: vector, image
     --distance       Point/Linestring buffer distance
 -i, --imagedir       Image data output directory
 -p, --pbffile        Pbf file
 -r, --rasterfile     Raster file
 -v, --vectordir      Vector data output directory
     --vectorfile     Vector file
     --vectorinput    Vector input file format: shapefile, geojson, wkt
     --vectoroutput   Vector output file format: geojson, csv
```

* e.g. extract all the vector data of the pbf file in geojson format in osmx directory
```
./run.sh -p riverside-latest.osm.pbf --dataformat vector --vectoroutput geojson -v osmx
```
* e.g. extract buildings and parks satellite image data with raster file HYP_LR.tif in osm-images directory
```
./run.sh -p riverside-latest.osm.pbf --dataset buildings,parks --dataformat image \
   --distance 0.1 --imagedir osm-images --rasterfile HYP_LR.tif
```
* e.g. extract image data with vector file osm21_lakes.csv and raster file HYP_LR.tif in osm-images directory
```
./run.sh --dataformat image --imagedir osm-images -r HYP_LR.tif --vectorfile osm21_lakes.csv --vectorinput wkt
```
* e.g. extract image data with shapefile file osm21_lakes.zip and raster file directory raster_dir
```
./run.sh --dataformat image -r raster_dir --vectorfile osm21_lakes.zip --vectorinput shapefile
```

2. Docker
```shell
docker pull zhangyaming0726/java:8.1
docker run -it <image id> /bin/bash
cd /usr/local/osm-extractor
wget https://download.geofabrik.de/antarctica-latest.osm.pbf
./run.sh antarctica-latest.osm.pbf geojson antarctica-extract
```

## Output
The output is a set of CSV or GeoJSON files according to the second parameter.
For example, GeoJSON output looks like the following.
```json
{
  "type" : "Feature",
  "properties" : {
    "id" : 751,
    "version" : 10,
    "timestamp" : 1449245143000,
    "changeSetId" : 35750798,
    "uid" : 395141,
    "uname" : "Tractor",
    "tagsMap" : {
      "junction" : "yes",
      "name" : "Majorstukrysset",
      "highway" : "traffic_signals"
    }
  },
  "geometry" : {
    "type" : "Point",
    "coordinates" : [ 10.716694200000001, 59.9292626 ]
  }
}
```

## Performance
We extracted a planet file (entire world) with a size of 58 GB on an Amazon AWS with
40 m5.4xlarge instance.
Each node is equipped with 16 cores, 64 GB of RAM, and 256 GB SSD storage.
The cluster was able to process the entire file in 4,412 seconds which makes
the total cost at about $60 which is a very small price to process the entire planet file.