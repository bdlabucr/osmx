package edu.ucr.cs.bdlab

import edu.ucr.cs.bdlab.beast.RasterReadMixinFunctions
import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.SpatialRDD
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.IFeature
import edu.ucr.cs.bdlab.beast.io.{SpatialReader, SpatialWriter}
import edu.ucr.cs.bdlab.raptor.{RaptorJoin, RaptorJoinResult}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.LongWritable
import org.apache.spark.beast.SparkSQLRegistration
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, lit, udf}
import org.apache.spark.sql.types.{DataTypes, LongType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Encoders, SparkSession, functions}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.locationtech.jts.geom.{Envelope, Geometry}
import org.openstreetmap.osmosis.core.domain.v0_6.{Entity, Node, Relation, Way}

import java.awt.Color
import java.awt.geom.Point2D
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import scala.Console.GREEN

object helpers {
  /**
   * Get SparkConf
   *
   * @return SparkConf
   */
  def getSparkConf: SparkConf = {
    val sparkConf = new SparkConf
    sparkConf.setAppName("OSM-Extractor")
    if (!sparkConf.contains("spark.master")) sparkConf.setMaster("local[*]")
    sparkConf
  }

  /**
   * Get SparkSession instance and register User-Defined Functions
   *
   * @return SparkSession
   */
  def getSparkSession(sparkConf: SparkConf): SparkSession = {
    val spark = SparkSession.builder.config(sparkConf).appName("OSM-Extractor").getOrCreate
    // Set spark log level
    spark.sparkContext.setLogLevel("WARN")
    // Create UDF to flatten longitude and latitude for SparkSQL functions in Beast
    spark.sqlContext.udf.register("mergePos", (lng: Double, lat: Double) => {
      def foo(lng: Double, lat: Double) = {
        val res = new Array[Double](2)
        res(0) = lng
        res(1) = lat
        res
      }

      foo(lng, lat)
    }, DataTypes.createArrayType(DataTypes.DoubleType))
    // Create UDF to convert geometry to WKT format
    spark.sqlContext.udf.register("covGeom", (geometry: Geometry) => geometry.toText, DataTypes.StringType)
    // Create UDF to convert tags (Map format) to String format
    spark.sqlContext.udf.register("covTags", (tagsMap: Map[_, _]) => tagsMap.mkString("\"", ",", "\""),
      DataTypes.StringType)
    spark.sqlContext.udf.register("getGeometryType", (geometry: Geometry) => geometry.getGeometryType, DataTypes.StringType)
    spark.sqlContext.udf.register("getArea", (geometry: Geometry) => geometry.getArea, DataTypes.DoubleType)
    spark.sqlContext.udf.register("getLength", (geometry: Geometry) => geometry.getLength, DataTypes.DoubleType)
    spark.sqlContext.udf.register("getNumGeometries", (geometry: Geometry) => geometry.getNumGeometries, DataTypes.IntegerType)
    spark.sqlContext.udf.register("getNumPoints", (geometry: Geometry) => geometry.getNumPoints, DataTypes.IntegerType)
    spark.sqlContext.udf.register("getDimension", (geometry: Geometry) => geometry.getDimension, DataTypes.IntegerType)
    spark.sqlContext.udf.register("getBoundaryDimension", (geometry: Geometry) => geometry.getBoundaryDimension, DataTypes.IntegerType)
    spark.sqlContext.udf.register("isSimple", (geometry: Geometry) => if (geometry.isSimple) 1 else 0, DataTypes.IntegerType)
    spark.sqlContext.udf.register("huMoments1", (geometry: Geometry) => getHuMoments(geometry, 1), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments2", (geometry: Geometry) => getHuMoments(geometry, 2), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments3", (geometry: Geometry) => getHuMoments(geometry, 3), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments4", (geometry: Geometry) => getHuMoments(geometry, 4), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments5", (geometry: Geometry) => getHuMoments(geometry, 5), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments6", (geometry: Geometry) => getHuMoments(geometry, 6), DataTypes.DoubleType)
    spark.sqlContext.udf.register("huMoments7", (geometry: Geometry) => getHuMoments(geometry, 7), DataTypes.DoubleType)


    // import Beast spark UDF
    SparkSQLRegistration.registerUDT
    SparkSQLRegistration.registerUDF(spark)
    spark
  }

  def getHuMoments(geometry: Geometry, n: Int): Double = {
    val coords = geometry.getCoordinates
    val image = Array.ofDim[Double](coords.length, 2)
    for (i <- 0 until coords.length) {
      image(i)(0) = coords(i).x
      image(i)(1) = coords(i).y
    }
    HuMoments.getHuMoment(image, n)
  }

  /**
   * Parsing pbf file using PbfEntityInputFormat.class
   *
   * @param sc      SparkContext
   * @param pbfFile the input pbf file name
   */
  def getEntities(sc: SparkContext, pbfFile: String): RDD[Entity] = {
    val rdd = sc.newAPIHadoopFile(pbfFile, classOf[PbfEntityInputFormat], classOf[LongWritable], classOf[Entity],
      sc.hadoopConfiguration)
    rdd.values.persist(StorageLevel.DISK_ONLY)
  }

  /**
   * Filter pbf entities and return all nodes Dataframe
   *
   * @return Dataset
   */
  def getNodeDf(spark: SparkSession, entities: RDD[Entity]): DataFrame = {
    // Filter Node DataFrame
    val osmNodes = entities.
      filter(e => e.isInstanceOf[Node]).
      map(e => e.asInstanceOf[Node]).
      map(e => new NodeDf(e.getId, e.getVersion, e.getTimestamp.getTime, e.getChangesetId, e.getUser.getId,
        e.getUser.getName, e.getLatitude, e.getLongitude, e.getTags))
    spark.createDataFrame(osmNodes, classOf[NodeDf]).alias("nodeDf")
  }

  /**
   * create all nodes view
   */
  def createNodesTempView(nodeDf: DataFrame): Unit = {
    nodeDf.createOrReplaceTempView("nodes")
  }

  /**
   * get nodeDf with point geometry
   *
   * @param viewName
   * @return
   */
  def getNodeDfWithGeom(sparkSession: SparkSession, viewName: String): DataFrame = {
    sparkSession.sql(
      s"""SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap, ST_CreatePoint(longitude, latitude) as geometry
         FROM $viewName""")
  }

  /**
   * Filter dataset by checking if the row has any tag attributes
   */
  val hasTag: UserDefinedFunction = udf((tagsMap: Map[String, String], keys: String, values: String) => {
    var matched = false
    if (tagsMap != null) {
      if (values == "") {
        for ((k, _) <- tagsMap; if !matched) {
          if (keys.contains(k)) {
            matched = true
          }
        }
      } else {
        for ((k, v) <- tagsMap; if !matched) {
          if (keys.contains(k) && values.contains(v)) {
            matched = true
          }
        }
      }
    }
    matched
  })

  def pois(allNodes: DataFrame): DataFrame = {
    allNodes.filter(hasTag(allNodes("tagsMap"),
      lit("name,amenity,cuisine,highway,railway,barrier,power,noexit,man_name, manmade,light_rail," +
        "traffic_calming,turning_circle,natural,public_transport,entrane,building,emergence,tourism,shop," +
        "attraction,histroic,advertising,usage,addr:street,addr:housenumber"), lit("")))
  }

  /**
   * Filter pbf entities and return all ways Dataframe
   *
   * @return Dataset
   */
  def getWayDf(spark: SparkSession, entities: RDD[Entity]): DataFrame = {
    // Filter Way DataFrame
    val osmWays = entities.filter(e => e.isInstanceOf[Way]).
      map(e => e.asInstanceOf[Way]).
      map(e => new WayDf(e.getId, e.getVersion, e.getTimestamp.getTime, e.getChangesetId, e.getUser.getId,
        e.getUser.getName, e.getWayNodes, e.getTags))
    spark.createDataFrame(osmWays, classOf[WayDf]).alias("wayDf")
  }

  /**
   * Right outer join way Dataframe and node Dataframe
   * for getting all ways' coordinates and selecting dangled nodes
   *
   * @return Dataset
   */
  def joinWayNode(nodeDf: DataFrame, wayDf: DataFrame): DataFrame = {
    nodeDf.createOrReplaceTempView("nodeDf1")
    wayDf.createOrReplaceTempView("wayDf1")

    nodeDf.sparkSession.sql(
      """
    WITH explodedWays AS (
      SELECT way.id, way.version, way.timestamp, way.changeSetId, way.uid, way.uname, way.tagsMap,
       element_at(nodeIds, 1) as first_nodeId,
       element_at(nodeIds, -1) as last_nodeId,
       posexplode(nodeIds) AS (pos, nodeIdWay)
      FROM wayDf1 AS way
    )
    SELECT array(longitude, latitude) AS node_location, longitude, latitude,
       node.id as nodeId, node.version as nodeVersion, node.timestamp as nodeTimestamp,
       node.changeSetId as nodeChangeSetId, node.uid as nodeUid, node.uname as nodeUname, node.tagsMap as nodeTagsMap,
       way.id, way.version, way.timestamp, way.changeSetId, way.uid, way.uname, way.tagsMap, way.nodeIdWay,
       first_nodeId, last_nodeId, pos
    FROM nodeDf1 AS node LEFT OUTER JOIN explodedWays AS way
    ON node.id = way.nodeIdWay
    """)
  }

  /**
   * Select all ways by join result's way Id is NOT Null
   * Prepare for following SparkSQL function ST_CreateLinePolygon
   */
  def createWayTempView(joinWayDf: DataFrame): Unit = {
    joinWayDf.createOrReplaceTempView("joinWayDf")
    joinWayDf.sparkSession.sql(
      """
        SELECT id, first(version) as version, first(timestamp) as timestamp, first(changeSetId) as changeSetId,
               first(uid) as uid, first(uname) as uname, first(tagsMap) as tagsMap,
               first(first_nodeId) AS first_nodeId,
               first(last_nodeId) AS last_nodeId,
               collect_list(nodeId) AS nodeIds,
               flatten(collect_list(node_location)) AS node_locations,
               collect_list(pos) AS poss
             FROM joinWayDf
             WHERE id IS NOT NULL
             GROUP BY id
      """)
      .createOrReplaceTempView("ways")

    // using WINDOW function
    //    joinWayDf.sparkSession.sql(
    //      """
    //        SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap,
    //               collect_list(nodeId) OVER w AS nodeIds,
    //               collect_list(node_location) OVER w AS node_locations,
    //               first(nodeId) OVER w AS first_nodeId,
    //               last(nodeId) OVER w AS last_nodeId,
    //               RANK() OVER w AS rank
    //             FROM joinWayDf
    //             WHERE id IS NOT NULL
    //             WINDOW w AS (PARTITION BY id ORDER BY pos)
    //      """)
    //      .createOrReplaceTempView("joinWayDf123")
    //
    //    // Reverse the rank col to get the correct collect_list() array
    //    joinWayDf.sparkSession.sql(
    //      """
    //         WITH groupedWaysReverse AS (
    //         SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap, nodeIds, node_locations, first_nodeId,
    //           last_nodeId, RANK() OVER w AS rankDesc
    //         FROM joinWayDf123
    //         WINDOW w AS (PARTITION BY id ORDER BY rank desc))
    //         SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap, nodeIds,
    //           flatten(node_locations) as node_locations, first_nodeId, last_nodeId
    //         FROM groupedWaysReverse
    //         WHERE rankDesc = 1
    //        """)
    //      .createOrReplaceTempView("ways")
  }

  /**
   * getWayDfWithGeom using ST_CreateLinePolygon
   *
   * @param sparkSession
   * @return
   */
  def getWayDfWithGeom(sparkSession: SparkSession): DataFrame = {
    sparkSession.sql(
      """
      SELECT id as wayId, version as wayVersion, timestamp as wayTimestamp,
        changeSetId as wayChangeSetId, uid as wayUid, uname as wayUname, tagsMap as wayTagsMap, first_nodeId,
        last_nodeId, nodeIds, size(nodeIds) as nodeIdsSize,
        ST_CreateLinePolygon(node_locations, nodeIds, poss) as geometry
      FROM ways
      """)
  }

  // get road_network dataframe
  def road_network(allWays: DataFrame): DataFrame = {
    allWays.filter(hasTag(allWays("wayTagsMap"), lit("highway,junction,ford,route,cutting,tunnel,amenity"),
      lit("yes,street,highway,service,parking_aisle,motorway,motorway_link,trunk,trunk_link,primary," +
        "primary_link,secondary,secondary_link,tertiary,tertiary_link,living_street,residential,unclassified," +
        "track,road,roundabout,escape,mini_roundabout,motorway_junction,passing_place,rest_area,turning_circle," +
        "detour,parking_entrance")))
  }

  /**
   * get all_ways df
   *
   * @param roadsDf
   * @return
   */
  def getRoads(roadsDf: DataFrame): DataFrame = {
    roadsDf.createOrReplaceTempView("roadsDf588")
    roadsDf.sparkSession.sql(
      """
      SELECT wayId AS id, wayVersion AS version, wayTimestamp AS timestamp,
          wayChangeSetId AS changeSetId, wayUid AS uid, wayUname AS uname,
          wayTagsMap AS tagsMap, geometry
      FROM roadsDf588
      """
    )
  }

  /**
   * get road_network
   *
   * @param roadsDf
   * @return
   */
  def getRoadNetwork(roadsDf: DataFrame): DataFrame = {
    roadsDf.createOrReplaceTempView("roadsDf589")
    roadsDf.sparkSession.sql(
      """
    WITH explodedRoads AS (
      SELECT wayId AS id, wayVersion AS version, wayTimestamp AS timestamp,
          wayChangeSetId AS changeSetId, wayUid AS uid, wayUname AS uname,
          wayTagsMap AS tagsMap,
          posexplode(arrays_zip(slice(nodeIds, 1, nodeIdsSize - 1), slice(nodeIds, 2, nodeIdsSize - 1), ST_BreakLine(geometry))) AS (pos, exp)
      FROM roadsDf589
      WHERE nodeIdsSize > 1
    )
    SELECT id, concat_ws('_', id, pos) as segmentId, exp.`0` as firstNodeId,
        exp.`1` as lastNodeId,
        exp.`2` as geometry,
        version, timestamp, changeSetId, uid, uname, tagsMap
    FROM explodedRoads
      """
    )
  }

  /**
   * Select Dangled Nodes by join result's way Id is Null
   */
  def createDangledNodesTempView(joinWayDf: DataFrame): Unit = {
    joinWayDf.sparkSession.sql(
      """
         SELECT nodeId as id, nodeVersion as version, nodeTimestamp as timestamp, nodeChangeSetId as changeSetId,
             nodeUid as uid, nodeUname as uname, nodeTagsMap as tagsMap, longitude, latitude
             FROM joinWayDf
             WHERE id IS NULL
         """)
      .createOrReplaceTempView("dangledNodes")
  }

  /**
   * Filter pbf entities and return all relations Dataframe
   *
   * @return Dataset
   */
  def getRelationDf(sparkSession: SparkSession, entities: RDD[Entity]): DataFrame = {
    // Filter Relation DataFrame
    val osmRelations = entities.
      filter(e => e.isInstanceOf[Relation]).
      map(e => e.asInstanceOf[Relation]).
      map(e => new RelationDf(e.getId, e.getVersion, e.getTimestamp.getTime, e.getChangesetId, e.getUser.getId,
        e.getUser.getName, e.getMembers, e.getTags))
    sparkSession.createDataFrame(osmRelations, classOf[RelationDf]).alias("relationDf")
  }

  /**
   * Right outer join relation Dataframe and way Dataframe
   * for getting all relations' geometries and selecting dangled ways
   *
   * @return Dataset
   */
  def joinRelationWay(relationDf: DataFrame, wayDf: DataFrame): DataFrame = {
    relationDf.createOrReplaceTempView("relationsDf11")
    wayDf.createOrReplaceTempView("wayDf11")
    relationDf.sparkSession.sql(
      """
         WITH explodedRelations AS (
           SELECT explode(arrays_zip(memberIds, entityTypes, memberRoles)) AS exp, *
           FROM relationsDf11
         )
         SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap, exp.entityTypes AS entityTypes,
           wayId, wayVersion, wayTimestamp, wayChangeSetId, wayUid, wayUname, wayTagsMap, first_nodeId, last_nodeId, geometry
         FROM explodedRelations RIGHT OUTER JOIN wayDf11
         ON exp.memberIds = wayId AND exp.entityTypes = "Way"
      """
    )
  }

  /**
   * Select Dangled Ways by join result's relation Id is Null
   */
  def getDangledWays(joinRelationWay: DataFrame): DataFrame = {
    // Select ways with null relation_id (ones that did not join with any relations)
    joinRelationWay.createOrReplaceTempView("joinRelationWay123")
    joinRelationWay.sparkSession.sql(
      """
         SELECT wayId AS id, wayVersion AS version, wayTimestamp AS timestamp, wayChangeSetId AS changeSetId,
           wayUid AS uid, wayUname AS uname, wayTagsMap AS tagsMap, geometry
         FROM joinRelationWay123
         WHERE id IS NULL
        """)
  }

  /**
   * Select all relations by join result's relation Id is NOT Null
   * Prepare for following SparkSQL function ST_Connect
   */
  def createRelationTempView(joinRelationWay: DataFrame): Unit = {
    joinRelationWay.createOrReplaceTempView("joinRelationWay456")
    joinRelationWay.sparkSession.sql(
      """
        SELECT id, first(version) AS version, first(timestamp) AS timestamp, first(changeSetId) AS changeSetId,
         first(uid) AS uid, first(uname) AS uname, first(tagsMap) AS tagsMap,
          collect_list(first_nodeId) AS first_nodeIds, collect_list(last_nodeId) AS last_nodeIds,
          collect_list(geometry) AS geometry
        FROM joinRelationWay456
        WHERE id IS NOT NULL
        GROUP BY id
        """)
      .createOrReplaceTempView("relations")
  }

  /**
   * getRelationDf
   *
   * @return DataFrame
   */
  def getRelationDfWithGeom(sparkSession: SparkSession): DataFrame = {
    // Import Beast features
    sparkSession.sql(
      """SELECT id, version, timestamp, changeSetId, uid, uname, tagsMap,
        ST_Connect(first_nodeIds, last_nodeIds, geometry) as geometry
        FROM relations""")
  }

  /**
   * Get all Dangled Nodes, Dangled Ways and Relations for following dataset splitting
   *
   * @return Dataset
   */
  def getAllObjects(all_nodes: DataFrame, all_ways: DataFrame, all_relations: DataFrame): DataFrame = {
    // all_objects datasets
    // Store the full dataset in DISK_ONLY level for reusing
    all_nodes.union(all_ways).union(all_relations).persist(StorageLevel.DISK_ONLY)
  }

  def buildings(allObjects: DataFrame): DataFrame = {
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("building"), lit("")))
  }

  def lakes(allObjects: DataFrame): DataFrame = {
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("natural,waterway"),
      lit("bay,wetland,water,coastline,riverbank,dock,boatyard,river,canal,stream,drain,ditch,pressurised,tidal_channel")))
  }

  def parks(allObjects: DataFrame): DataFrame = {
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("leisure,boundary,landuse,natural"),
      lit("wood,tree_row,grassland,park,golf_course,national_park,garden,nature_reserve,forest,grass,tree," +
        "orchard,farmland,protected_area")))
  }

  def cemetery(allObjects: DataFrame): DataFrame = {
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("landuse"), lit("cemetery")))
  }

  def sports(allObjects: DataFrame): DataFrame = {
    //    allObjects.filter(hasTag(allObjects("tagsMap"), lit("leisure,sport"),
    //      lit("sports_centre,stadium,track,pitch,golf_course,water_park,swimming_pool,recreation_ground,piste")))
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("sport"), lit("")))
  }

  def postal_codes(allObjects: DataFrame): DataFrame = {
    allObjects.filter(hasTag(allObjects("tagsMap"), lit("boundary"), lit("administrative")))
  }

  /**
   * Save Dataframe to .csv.bz2 file
   *
   * @param df        dataframe
   * @param directory output directory
   */
  def dfToCsv(df: DataFrame, directory: String): Unit = {
    var dfTemp = df.withColumn("tags", functions.callUDF("covTags", df.col("tagsMap"))).
      withColumn("geometry", functions.callUDF("covGeom", df.col("geometry")))
    if (directory.endsWith("road_network")) {
      dfTemp = dfTemp.select("geometry", "id", "segmentId", "firstNodeId", "lastNodeId", "version", "timestamp", "uid", "uname", "tags")
    } else {
      dfTemp = dfTemp.select("geometry", "id", "version", "timestamp", "uid", "uname", "tags")
    }
    dfTemp.
      coalesce(1).
      write.
      mode("overwrite").
      option("header", "true").
      option("delimiter", "\t").
      option("quote", "").
      option("compression", "bzip2").
      csv(directory)
  }

  /**
   * Save Dataframe to .geojson file
   *
   * @param df        dataframe
   * @param directory output directory
   */
  def dfToGeoJson(df: DataFrame, directory: String): Unit = {
    SpatialWriter.saveFeatures(SpatialReader.dataFrameToSpatialRDD(df), "geojson", directory, new BeastOptions)
  }

  /**
   * vector export
   */
  def vectorExport(option: String, df: DataFrame, directory: String): Unit = {
    val start = System.currentTimeMillis()
    if (option == "csv") {
      dfToCsv(df, directory)
    } else if (option == "geojson") {
      dfToGeoJson(df, directory)
    }
    val end = System.currentTimeMillis()
    Console.println(s"${
      GREEN
    }${
      directory
    } finished in ${
      ((end - start) / 1000.0).formatted("%.3f")
    } s")
  }

  /**
   * image exporter
   *
   * @param df Dataframe with vector data
   */
  def imageExport(sparkSession: SparkSession, spatialRdd: SpatialRDD, df: DataFrame, raster: String, distant: Double, dir: String): Unit = {
    val outputResolution = 256
    val keepAspectRatio = true
    // TODO get buffer geometry of point or linestring in vector file
    //    var buffer: DataFrame = null
    //    if (spatialRdd != null) {
    //      val geometryRDD = spatialRdd.map(f => {
    //        val id = f.getAs[Any]("id").toString.toLong
    //        val geometry = f.getAs[Geometry]("g")
    //        if (geometry.getGeometryType == Geometry.TYPENAME_POINT) {
    //          new GeometryDf(id, geometry.buffer(distant))
    //        } else if (geometry.getGeometryType == Geometry.TYPENAME_LINESTRING) {
    //          new GeometryDf(id, geometry.buffer(distant, 4))
    //        } else {
    //          new GeometryDf(id, geometry)
    //        }
    //      })
    //      buffer = sparkSession.createDataFrame(geometryRDD, classOf[GeometryDf]).toDF("id", "geometry")
    //    } else {
    //      buffer = df.map(x => {
    //        val geometry = x.getAs[Geometry]("geometry")
    //        if (geometry.getGeometryType == Geometry.TYPENAME_POINT) {
    //          (x.getLong(0), geometry.buffer(distant))
    //        } else if (geometry.getGeometryType == Geometry.TYPENAME_LINESTRING) {
    //          (x.getLong(0), geometry.buffer(distant, 4))
    //        } else {
    //          (x.getLong(0), geometry)
    //        }
    //      })(Encoders.product[(Long, Geometry)]).toDF("id", "geometry")
    //    }
    //    val vector = SpatialReader.dataFrameToSpatialRDD(buffer)
    //    val geometries = vector.map(x => (x.getLong(0), x))
    var geometries: RDD[(Long, IFeature)] = null
    if (spatialRdd != null) {
      if (spatialRdd.first().schema.contains(StructField("id", LongType))) {
        geometries = spatialRdd.map(f => (f.getAs[Any]("id").toString.toLong, f))
      } else {
        geometries = spatialRdd.zipWithUniqueId().map(f => (f._2, f._1))
      }
    } else {
      val buffer = df.map(x => {
        val geometry = x.getAs[Geometry]("geometry")
        if (geometry.getGeometryType == Geometry.TYPENAME_POINT) {
          (x.getLong(0), geometry.buffer(distant))
        } else if (geometry.getGeometryType == Geometry.TYPENAME_LINESTRING) {
          (x.getLong(0), geometry.buffer(distant, 4))
        } else {
          (x.getLong(0), geometry)
        }
      })(Encoders.product[(Long, Geometry)]).toDF("id", "geometry")
      val vector = SpatialReader.dataFrameToSpatialRDD(buffer)
      geometries = vector.map(x => (x.getLong(0), x))
    }

    val elevation = sparkSession.sparkContext.geoTiff(raster)

    val joinResults: RDD[RaptorJoinResult[Array[Int]]] =
      RaptorJoin.raptorJoinIDFull(elevation, geometries, new BeastOptions())
    val countryMBRs = geometries.map(x => (x._1, x._2.getGeometry.getEnvelopeInternal))

    val joinResultsWithMBR = joinResults
      .map(result => {
        val pixelLocation = new Point2D.Double
        result.rasterMetadata.gridToModel(result.x, result.y, pixelLocation)
        val x = pixelLocation.x
        val y = pixelLocation.y
        result.rasterMetadata.gridToModel(result.x + 1.0, result.y + 1.0, pixelLocation)
        val color = new Color(result.m(0), result.m(1), result.m(2)).getRGB
        (result.featureID, (x.toFloat, y.toFloat, pixelLocation.x.toFloat, pixelLocation.y.toFloat, color))
      })
      .join(countryMBRs)

    val emptyPixels: Array[Int] = new Array[Int](outputResolution * outputResolution)
    val countryPixels: RDD[(Long, Array[Int])] = joinResultsWithMBR
      .aggregateByKey(emptyPixels)((pixels, result) => {
        val mbr = result._2
        // Map the pixel boundaries to the target image and color all target pixels with the pixel color
        // Notice that some pixels might be partially outside the polygon boundaries because the Raptor join
        // operation finds pixels with a center inside the polygon not the entire pixel inside the polygon
        var xRatio = outputResolution / mbr.getWidth
        var yRatio = outputResolution / mbr.getHeight
        if (keepAspectRatio) {
          xRatio = xRatio min yRatio
          yRatio = xRatio
        }
        val x1 = ((result._1._1 - mbr.getMinX) * xRatio).toInt max 0
        val y1 = (outputResolution - 1 - ((result._1._2 - mbr.getMinY) * yRatio)).toInt max 0
        val x2 = ((result._1._3 - mbr.getMinX) * xRatio).toInt min (outputResolution - 1)
        val y2 = (outputResolution - 1 - ((result._1._4 - mbr.getMinY) * yRatio).toInt) min (outputResolution - 1)
        for (x <- x1 until x2; y <- y1 until y2) {
          val offset = y * outputResolution + x
          pixels(offset) = result._1._5
        }
        pixels
      }, (pixels1, pixels2) => {
        for (i <- pixels1.indices; if pixels1(i) == 0)
          pixels1(i) = pixels2(i)
        pixels1
      })

    countryPixels.foreach(cpixels => {
      val image = new BufferedImage(outputResolution, outputResolution, BufferedImage.TYPE_INT_ARGB)
      for (x <- 0 until outputResolution; y <- 0 until outputResolution) {
        val offset = y * outputResolution + x
        image.setRGB(x, y, cpixels._2(offset))
      }
      // Write the image to the output
      val imagePath = new Path(dir, cpixels._1 + ".png")
      val filesystem = imagePath.getFileSystem(new Configuration())
      val out = filesystem.create(imagePath)
      ImageIO.write(image, "png", out)
      out.close()
    })
  }

  def exportHelper(sparkSession: SparkSession, df: DataFrame, dataName: String, dataset: String, dataFormat: String,
                   vectorFormat: String, vectorDir: String, imageDir: String, rasterFile: String,
                   distance: Double, start: Long, datasetLength: Int): Int = {
    if (dataset.contains(dataName) || dataset == "") {
      if (dataFormat != "image") {
        vectorExport(vectorFormat, df, s"$vectorDir/$dataName.bz2")
        println(s"Exported $dataName after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
      }
      if (dataFormat != "vector") {
        imageExport(sparkSession, null, df, rasterFile, distance, s"$imageDir/$dataName")
      }
      if (dataset.contains(dataName) && datasetLength == 1) {
        System.exit(0)
      }
      datasetLength - 1
    } else {
      datasetLength
    }
  }

  def classification(): Unit = {
    //["airport", "airport_hangar", "airport_terminal", "amusement_park", "aquaculture", "archaeological_site", "barn", "border_checkpoint", "burial_site", "car_dealership", "construction_site", "crop_field", "dam", "debris_or_rubble", "educational_institution", "electric_substation", "factory_or_powerplant", "fire_station", "flooded_road", "fountain", "gas_station", "golf_course", "ground_transportation_station", "helipad", "hospital", "impoverished_settlement", "interchange", "lake_or_pond", "lighthouse", "military_facility", "multi-unit_residential", "nuclear_powerplant", "office_building", "oil_or_gas_facility", "park", "parking_lot_or_garage", "place_of_worship", "police_station", "port", "prison", "race_track", "railway_bridge", "recreational_facility", "road_bridge", "runway", "shipyard", "shopping_mall", "single-unit_residential", "smokestack", "solar_farm", "space_facility", "stadium", "storage_tank", "surface_mine", "swimming_pool", "toll_booth", "tower", "tunnel_opening", "waste_disposal", "water_treatment_facility", "wind_farm", "zoo"]

    var df = getSparkSession(getSparkConf).read.format("csv")
      .option("sep", ",")
      .option("inferSchema", "true")
      .option("header", "true")
      .load("ca/ml_df.csv")

    val building = df
      .filter(col("dataset").equalTo("buildings") &&
        col("geometryType").equalTo("Polygon"))
      .randomSplit(Array(25603.toDouble / 317940, 292337.toDouble / 317940))(0)

    val lakes = df
      .filter(col("dataset").equalTo("lakes") &&
        col("geometryType").equalTo("Polygon"))

    val parks = df
      .filter(col("dataset").equalTo("parks") &&
        col("geometryType").equalTo("Polygon"))
      .randomSplit(Array(25603.toDouble / 125052, 99449.toDouble / 317940))(0)

    df = building.union(lakes).union(parks)

    val indexer = new StringIndexer()
      .setInputCols(Array("dataset", "geometryType"))
      .setOutputCols(Array("label", "geometryTypeInt"))
      .setHandleInvalid("skip")

    // point line polygon how to extract from osm
    val assembler = new VectorAssembler()
      .setInputCols(Array(
        "geometryTypeInt"
        , "area"
        , "length"
        , "numGeometries"
        , "numPoints"
        , "dimension"
        , "boundaryDimension"
        , "isSimple"
        , "huMoments1", "huMoments2", "huMoments3", "huMoments4", "huMoments4", "huMoments5", "huMoments6", "huMoments7"
      ))
      .setOutputCol("features")

    val lr = new LogisticRegression()

    val pipeline = new Pipeline()
      .setStages(Array(indexer, assembler, lr))

    val paramGrid = new ParamGridBuilder()
      //      .addGrid(hashingTF.numFeatures, Array(10, 100, 1000))
      .addGrid(lr.regParam, Array(0.01, 0.1, 0.3, 0.8))
      .build()

    val cv = new TrainValidationSplit()
      .setEstimator(pipeline)
      .setEvaluator(new MulticlassClassificationEvaluator())
      .setEstimatorParamMaps(paramGrid)
      .setTrainRatio(0.8)
      .setParallelism(2)

    val Array(trainingData, testData) = df.randomSplit(Array(0.8, 0.2))

    // start timestamp
    val start = System.currentTimeMillis()
    val logisticModel = cv.fit(trainingData)
    var end = System.currentTimeMillis()
    Console.println(s"${GREEN}Training finished in ${((end - start) / 1000.0).formatted("%.3f")} s")

    //        val numFeatures = logisticModel.bestModel.asInstanceOf[PipelineModel].stages(1).asInstanceOf[HashingTF].getNumFeatures
    //        val regParam = logisticModel.bestModel.asInstanceOf[PipelineModel].stages(3).asInstanceOf[LogisticRegressionModel].getRegParam
    //        println(s"numFeatures: $numFeatures")
    //        println(s"regParam: $regParam")
    // not classified do not know how to no tags/only name
    // extract

    val predictions = logisticModel.transform(testData)
    predictions.select("label", "prediction", "probability").show(false)

    val multiclassClassificationEvaluator = new MulticlassClassificationEvaluator()
      .setLabelCol("label")
      .setPredictionCol("prediction")

    val accuracy = multiclassClassificationEvaluator.evaluate(predictions)
    println(s"Accuracy of the test set is $accuracy")

    end = System.currentTimeMillis()
    Console.println(s"${GREEN}Process finished in ${((end - start) / 1000.0).formatted("%.3f")} s")
  }

  def help(): Unit = {
    println("USAGE: ./run.sh <pbf file> [options...]")
    println(" -d, --dataset        Dataset: all_nodes, pois, roads, road_network, all_objects")
    println("                      buildings, lakes, parks, cemetery, sports, postal_codes")
    println("     --dataformat     Output format: vector, image")
    println("     --distance       Point/Linestring buffer distance")
    println(" -i, --imagedir       Image data output directory")
    println(" -p, --pbffile        Pbf file")
    println(" -r, --rasterfile     Raster file")
    println(" -v, --vectordir      Vector data output directory")
    println("     --vectorfile     Vector file")
    println("     --vectorinput    Vector input file format: shapefile, geojson, wkt")
    println("     --vectoroutput   Vector output file format: geojson, csv")
    System.exit(0)
  }
}
