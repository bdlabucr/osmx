//package edu.ucr.cs.bdlab
//
//import org.apache.spark.sql.DataFrame
//import org.apache.spark.sql.expressions.UserDefinedFunction
//import org.apache.spark.sql.functions.{lit, udf}
//
//object planet_extractor {
//  val allNodes: DataFrame = OsmExtractor.getNodeDf("nodes")
//  val allWaysDf: DataFrame = OsmExtractor.getWayDf
//  val allWays: DataFrame = allWaysDf
//    .select(allWaysDf("wayId").alias("id"),
//      allWaysDf("wayVersion").alias("version"),
//      allWaysDf("wayTimestamp").alias("timestamp"),
//      allWaysDf("wayChangeSetId").alias("changeSetId"),
//      allWaysDf("wayUid").alias("uid"),
//      allWaysDf("wayUname").alias("uname"),
//      allWaysDf("wayTagsMap").alias("tagsMap"),
//      allWaysDf("geometry"))
//  val allObjects: DataFrame = OsmExtractor.getOsmPbfReader.getAllObjects
//
//  /**
//   * Filter dataset by checking if the row has any tag attributes
//   */
//  val hasTag: UserDefinedFunction = udf((tagsMap: Map[String, String], keys: String, values: String) => {
//    var matched = false
//    if (tagsMap != null) {
//      if (values == "") {
//        for ((k, _) <- tagsMap; if !matched) {
//          if (keys.contains(k)) {
//            matched = true
//          }
//        }
//      } else {
//        for ((k, v) <- tagsMap; if !matched) {
//          if (keys.contains(k) && values.contains(v)) {
//            matched = true
//          }
//        }
//      }
//    }
//    matched
//  })
//
//  def pois: DataFrame = {
//    allNodes.filter(hasTag(allNodes("tagsMap"),
//      lit("name,amenity,cuisine,highway,railway,barrier,power,noexit,man_name, manmade,light_rail," +
//        "traffic_calming,turning_circle,natural,public_transport,entrane,building,emergence,tourism,shop," +
//        "attraction,histroic,advertising,usage,addr:street,addr:housenumber"), lit("")))
//  }
//
//  def road_network: DataFrame = {
//    allWays.filter(hasTag(allWays("tagsMap"), lit("highway,junction,ford,route,cutting,tunnel,amenity"),
//      lit("yes,street,highway,service,parking_aisle,motorway,motorway_link,trunk,trunk_link,primary," +
//        "primary_link,secondary,secondary_link,tertiary,tertiary_link,living_street,residential,unclassified," +
//        "track,road,roundabout,escape,mini_roundabout,motorway_junction,passing_place,rest_area,turning_circle," +
//        "detour,parking_entrance")))
//  }
//
//  def buildings: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("amenity,building"), lit("yes")))
//  }
//
//  def lakes: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("natural,waterway"),
//      lit("bay,wetland,water,coastline,riverbank,dock,boatyard")))
//  }
//
//  def parks: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("leisure,boundary,landuse,natural"),
//      lit("wood,tree_row,grassland,park,golf_course,national_park,garden,nature_reserve,forest,grass,tree," +
//        "orchard,farmland,protected_area")))
//  }
//
//  def cemetery: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("landuse"), lit("cemetery")))
//  }
//
//  def sports: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("leisure,landuse"),
//      lit("sports_centre,stadium,track,pitch,golf_course,water_park,swimming_pool,recreation_ground,piste")))
//  }
//
//  def postal_codes: DataFrame = {
//    allObjects.filter(hasTag(allObjects("tagsMap"), lit("admin_level"), lit("8")))
//  }
//}
