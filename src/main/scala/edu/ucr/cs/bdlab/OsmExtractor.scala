package edu.ucr.cs.bdlab

import edu.ucr.cs.bdlab.beast.{ReadWriteMixinFunctions, SpatialRDD}
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.storage.StorageLevel

import scala.Console.{GREEN, RED}

/**
 * Scala codes for osm-extractor
 * Using Beast SparkSQL Api
 */
object OsmExtractor {
  def main(args: Array[String]): Unit = {
    // start timestamp
    val start = System.currentTimeMillis()

    // check arguments
    if (args.length == 0) {
      helpers.help()
    }

    // read arguments
    var dataset = ""
    var datasetLength = 12
    var dataFormat = "vector"
    var distance = 0.1
    var imageDir = "osm-images"
    var pbfFile = ""
    var rasterFile = ""
    var vectorDir = "osmx"
    var vectorFile = ""
    var vectorInput = ""
    var vectorOutput = "csv"
    var i = 0
    while (i < args.length - 1) {
      if (args(i) == "-d" || args(i) == "--dataset") {
        dataset = args(i + 1)
        datasetLength = dataset.split(",").length
      } else if (args(i) == "--dataformat") {
        dataFormat = args(i + 1)
      } else if (args(i) == "--distance") {
        distance = args(i + 1).toDouble
      } else if (args(i) == "-i" || args(i) == "--imagedir") {
        imageDir = args(i + 1)
      } else if (args(i) == "-p" || args(i) == "--pbffile") {
        pbfFile = args(i + 1)
      } else if (args(i) == "-r" || args(i) == "--rasterfile") {
        rasterFile = args(i + 1)
      } else if (args(i) == "-v" || args(i) == "--vectordir") {
        vectorDir = args(i + 1)
      } else if (args(i) == "--vectorfile") {
        vectorFile = args(i + 1)
      } else if (args(i) == "--vectorinput") {
        vectorInput = args(i + 1)
      } else if (args(i) == "--vectoroutput") {
        vectorOutput = args(i + 1)
      } else {
        helpers.help()
      }
      i += 2
    }
    if (dataFormat == "vector" && pbfFile == "" || dataFormat == "image" && rasterFile == "") {
      Console.println(s"${RED}Please type pbf file or raster file name as argument")
      helpers.help()
    }

    // init sparkSession
    val sparkConf = helpers.getSparkConf
      .set("spark.serializer", classOf[KryoSerializer].getName) // Use kryo serializer
      .set("spark.kryo.registrator", classOf[CustomKryoRegistrator].getName) // register entity classes
    val sparkSession = helpers.getSparkSession(sparkConf)

    if (vectorFile != "" && dataFormat == "image" && rasterFile != "") {
      var vector: SpatialRDD = null
      if (vectorInput == "shapefile") {
        vector = sparkSession.sparkContext.shapefile(vectorFile)
      } else if (vectorInput == "geojson") {
        vector = sparkSession.sparkContext.geojsonFile(vectorFile)
      } else if (vectorInput == "wkt") {
        vector = sparkSession.sparkContext.readWKTFile(vectorFile, "geometry")
      }
      helpers.imageExport(sparkSession, vector, null, rasterFile, distance, imageDir)
      System.exit(0)
    }

    // read pbf file and return entities
    val entities = helpers.getEntities(sparkSession.sparkContext, pbfFile)

    // get nodes dataframe and create view
    val nodeDf = helpers.getNodeDf(sparkSession, entities)
    helpers.createNodesTempView(nodeDf)

    // create all_nodes dataframe using ST_CreatePoint and export
    val all_nodes = helpers.getNodeDfWithGeom(sparkSession, "nodes")
    //helpers.exportHelper(outputFormat, all_nodes, s"$outputPath/all_nodes.bz2")
    //println(s"Exported all nodes after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, all_nodes, "all_nodes",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    // get pois dataframe and export
    val pois = helpers.pois(all_nodes)
    //helpers.vectorExport(vectorOutput, pois, s"$vectorDir/pois.bz2")
    //println(s"Exported pois after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, pois, "pois",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    // get way dataframe
    val wayDf = helpers.getWayDf(sparkSession, entities)

    // join way df with node df
    val joinWayDf = helpers.joinWayNode(nodeDf, wayDf)

    // create ways view
    helpers.createWayTempView(joinWayDf)

    // get allWaysDf with first and last nodes
    val allWaysDf = helpers.getWayDfWithGeom(sparkSession)

    // get road_network df
    val roadNetworkDf = helpers.road_network(allWaysDf)

    // get roads
    val roads = helpers.getRoads(roadNetworkDf)
    //helpers.vectorExport(vectorOutput, roads, s"$vectorDir/roads.bz2")
    //println(s"Exported roads ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, roads, "roads",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    // get road_network
    val road_network = helpers.getRoadNetwork(roadNetworkDf)
    //helpers.vectorExport(vectorOutput, road_network, s"$vectorDir/road_network.bz2")
    //println(s"Exported road_network ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, road_network, "road_network",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    // get dangled nodes dataframe and create view
    helpers.createDangledNodesTempView(joinWayDf)

    // get dangled nodes dataframe using ST_CreatePoint
    val dangled_nodes = helpers.getNodeDfWithGeom(sparkSession, "dangledNodes")

    // get relation df
    val relationDf = helpers.getRelationDf(sparkSession, entities)

    // join relation df with way df
    val joinRelationWayDf = helpers.joinRelationWay(relationDf, allWaysDf)

    // get dangled ways
    val dangled_ways = helpers.getDangledWays(joinRelationWayDf)

    // create relations view
    helpers.createRelationTempView(joinRelationWayDf)

    // get all relations
    val all_relations = helpers.getRelationDfWithGeom(sparkSession)

    // get all objects and export
    val all_objects = helpers.getAllObjects(dangled_nodes, dangled_ways, all_relations)
    // Store the full dataset in DISK_ONLY level for reusing
    all_objects.persist(StorageLevel.DISK_ONLY)
    //helpers.vectorExport(vectorOutput, all_objects, s"$vectorDir/all_objects.bz2")
    //println(s"Exported all objects after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, all_objects, "all_objects",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    // filter all the other features using all_objects
    val buildings = helpers.buildings(all_objects)
    //helpers.vectorExport(vectorOutput, buildings, s"$vectorDir/buildings.bz2")
    //println(s"Exported buildings after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, buildings, "buildings",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)
    val lakes = helpers.lakes(all_objects)
    //helpers.vectorExport(vectorOutput, lakes, s"$vectorDir/lakes.bz2")
    //println(s"Exported lakes after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, lakes, "lakes",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)
    val parks = helpers.parks(all_objects)
    //helpers.vectorExport(vectorOutput, parks, s"$vectorDir/parks.bz2")
    //println(s"Exported parks after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, parks, "parks",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)
    val cemetery = helpers.cemetery(all_objects)
    //helpers.vectorExport(vectorOutput, cemetery, s"$vectorDir/cemetery.bz2")
    //println(s"Exported cemetery after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, cemetery, "cemetery",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)
    val sports = helpers.sports(all_objects)
    //helpers.vectorExport(vectorOutput, sports, s"$vectorDir/sports.bz2")
    //println(s"Exported sports after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, sports, "sports",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)
    val postal_codes = helpers.postal_codes(all_objects)
    //helpers.vectorExport(vectorOutput, postal_codes, s"$vectorDir/postal_codes.bz2")
    //println(s"Exported postal codes after ${(System.currentTimeMillis() - start) * 1E-3} seconds")
    datasetLength = helpers.exportHelper(sparkSession, postal_codes, "postal_codes",
      dataset, dataFormat, vectorOutput, vectorDir, imageDir, rasterFile, distance, start, datasetLength)

    //    val building_label = buildings.withColumn("dataset", lit("buildings")).randomSplit(Array(333333.toDouble / 6515175, 6181842.toDouble / 6515175))(0)
    //    val lakes_label = lakes.withColumn("dataset", lit("lakes")).randomSplit(Array(333333.toDouble / 924415, 591082.toDouble / 924415))(0)
    //    val parks_label = parks.withColumn("dataset", lit("parks")).randomSplit(Array(333333.toDouble / 355160, 21827.toDouble / 355160))(0)
    //    //    val cemetery_label = cemetery.withColumn("dataset", lit("cemetery"))
    //    //    val sports_label = sports.withColumn("dataset", lit("sports"))
    //    //    val postal_codes_label = postal_codes.withColumn("dataset", lit("postal_codes"))
    //    var ml_df = building_label
    //      .union(lakes_label)
    //      .union(parks_label)
    //      .select("geometry", "dataset")
    //    ml_df = ml_df
    //      .withColumn("geometryType", functions.callUDF("getGeometryType", ml_df.col("geometry")))
    //      .withColumn("area", functions.callUDF("getArea", ml_df.col("geometry")))
    //      .withColumn("length", functions.callUDF("getLength", ml_df.col("geometry")))
    //      .withColumn("numGeometries", functions.callUDF("getNumGeometries", ml_df.col("geometry")))
    //      .withColumn("numPoints", functions.callUDF("getNumPoints", ml_df.col("geometry")))
    //      .withColumn("dimension", functions.callUDF("getDimension", ml_df.col("geometry")))
    //      .withColumn("boundaryDimension", functions.callUDF("getBoundaryDimension", ml_df.col("geometry")))
    //      .withColumn("isSimple", functions.callUDF("isSimple", ml_df.col("geometry")))
    //      .withColumn("huMoments1", functions.callUDF("huMoments1", ml_df.col("geometry")))
    //      .withColumn("huMoments2", functions.callUDF("huMoments2", ml_df.col("geometry")))
    //      .withColumn("huMoments3", functions.callUDF("huMoments3", ml_df.col("geometry")))
    //      .withColumn("huMoments4", functions.callUDF("huMoments4", ml_df.col("geometry")))
    //      .withColumn("huMoments5", functions.callUDF("huMoments5", ml_df.col("geometry")))
    //      .withColumn("huMoments6", functions.callUDF("huMoments6", ml_df.col("geometry")))
    //      .withColumn("huMoments7", functions.callUDF("huMoments7", ml_df.col("geometry")))
    //      .drop("geometry")
    //    ml_df
    //      .coalesce(1).
    //      write.
    //      mode("overwrite").
    //      option("header", "true").
    //      option("quote", "").
    //      csv("ml_df")
    //
    //    import org.apache.hadoop.fs._
    //    val fs = FileSystem.get(sparkSession.sparkContext.hadoopConfiguration)
    //    val file = fs.globStatus(new Path("ml_df/part*"))(0).getPath.getName
    //
    //    fs.rename(new Path("ml_df/" + file), new Path("ml_df/ml_df.csv"))
    //
    //    helpers.classification()

    // end timestamp
    val end = System.currentTimeMillis()
    Console.println(s"${GREEN}Extraction finished in ${((end - start) / 1000.0).formatted("%.3f")} s")
  }
}