package edu.ucr.cs.bdlab;

import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class EntityDf {
    private final long id;
    private final int version;
    private final long timestamp;
    private final long changeSetId;
    private final int uid;
    private final String uname;
    private final Map<String, String> tagsMap;

    public EntityDf(long id, int version, long timestamp, long changeSetId, int uid, String uname, Collection<Tag> tags) {
        this.id = id;
        this.version = version;
        this.timestamp = timestamp;
        this.changeSetId = changeSetId;
        this.uid = uid;
        this.uname = uname;
        this.tagsMap = new HashMap<>();
        for (Tag tag : tags) {
            this.tagsMap.put(
                    tag.getKey(), tag.getValue()
            );
        }
    }

    public Long getId() {
        return id;
    }

    public int getVersion() {
        return version;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getChangeSetId() {
        return changeSetId;
    }

    public int getUid() {
        return uid;
    }

    public String getUname() {
        return uname;
    }

    public Map<String, String> getTagsMap() {
        return tagsMap;
    }
}
