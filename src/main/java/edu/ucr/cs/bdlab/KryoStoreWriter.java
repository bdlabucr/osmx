/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab;

import com.esotericsoftware.kryo.io.Output;
import org.openstreetmap.osmosis.core.store.StoreWriter;

/**
 * A wrapper around Kryo Output to make it similar to StoreWriter
 */
public class KryoStoreWriter implements StoreWriter {

  private final Output output;

  public KryoStoreWriter(Output output) {
    this.output = output;
  }

  @Override
  public void writeBoolean(boolean b) {
    output.writeBoolean(b);
  }

  @Override
  public void writeByte(byte b) {
    output.writeByte(b);
  }

  @Override
  public void writeCharacter(char c) {
    output.writeChar(c);
  }

  @Override
  public void writeInteger(int i) {
    output.writeInt(i);
  }

  @Override
  public void writeLong(long l) {
    output.writeLong(l);
  }

  @Override
  public void writeDouble(double v) {
    output.writeDouble(v);
  }

  @Override
  public void writeString(String s) {
    output.writeString(s);
  }
}
