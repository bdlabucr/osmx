/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab;

import com.esotericsoftware.kryo.Kryo;
import org.apache.spark.serializer.KryoRegistrator;

public class CustomKryoRegistrator implements KryoRegistrator {
  @Override
  public void registerClasses(Kryo kryo) {
    EntitySerializer serializer = new EntitySerializer();
    kryo.register(org.openstreetmap.osmosis.core.domain.v0_6.Entity.class, serializer);
    kryo.register(org.openstreetmap.osmosis.core.domain.v0_6.Node.class, serializer);
    kryo.register(org.openstreetmap.osmosis.core.domain.v0_6.Way.class, serializer);
    kryo.register(org.openstreetmap.osmosis.core.domain.v0_6.Relation.class, serializer);
  }
}
