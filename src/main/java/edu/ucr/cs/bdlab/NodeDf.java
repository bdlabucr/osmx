package edu.ucr.cs.bdlab;

import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

import java.io.Serializable;
import java.util.Collection;

public class NodeDf extends EntityDf implements Serializable {
    private final double latitude;
    private final double longitude;

    public NodeDf(long id, int version, long timestamp, long changeSetId, int uid, String uname, double latitude, double longitude, Collection<Tag> tags) {
        super(id, version, timestamp, changeSetId, uid, uname, tags);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
