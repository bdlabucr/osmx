package edu.ucr.cs.bdlab;

import org.locationtech.jts.geom.Geometry;
import java.io.Serializable;

public class GeometryDf implements Serializable {
    private final long id;
    private final Geometry geometry;

    public GeometryDf(long id, Geometry geometry) {
        this.id = id;
        this.geometry = geometry;
    }

    public long getId() {
        return id;
    }

    public Geometry getGeometry() {
        return geometry;
    }
}
