package edu.ucr.cs.bdlab;

import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WayDf extends EntityDf implements Serializable {
    private final List<Long> nodeIds;

    public WayDf(long id, int version, long timestamp, long changeSetId, int uid, String uname, List<WayNode> wayNodes, Collection<Tag> tags) {
        super(id, version, timestamp, changeSetId, uid, uname, tags);
        nodeIds = new ArrayList<>();
        for (WayNode node : wayNodes) {
            this.nodeIds.add(node.getNodeId());
        }
    }

    public List<Long> getNodeIds() {
        return nodeIds;
    }
}
