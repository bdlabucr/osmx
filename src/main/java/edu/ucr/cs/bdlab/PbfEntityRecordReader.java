package edu.ucr.cs.bdlab;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
import org.openstreetmap.osmosis.osmbinary.Fileformat;
import org.openstreetmap.osmosis.pbf2.v0_6.impl.PbfBlobDecoder;
import org.openstreetmap.osmosis.pbf2.v0_6.impl.PbfBlobDecoderListener;
import org.openstreetmap.osmosis.pbf2.v0_6.impl.RawBlob;

import java.io.EOFException;
import java.io.IOException;
import java.util.List;

public class PbfEntityRecordReader extends RecordReader<LongWritable, Entity> implements PbfBlobDecoderListener {
    private static final int DEFAULT_BUFFER_SIZE = 64 * 1024;
    private long start;
    private long end;
    private long pos;
    private FSDataInputStream fileFD;
    protected int iCurrentEntity;
    private List<EntityContainer> currentEntityList;

    public void initialize(InputSplit genericSplit, TaskAttemptContext context) throws IOException {
        FileSplit split = (FileSplit) genericSplit;

        // Configure the external parameters - path, input
        Configuration conf = context.getConfiguration();
        final Path file = split.getPath();
        final FileSystem fs = file.getFileSystem(conf);

        start = split.getStart();
        end = start + split.getLength();
        pos = start;
        fileFD = fs.open(file);
    }

    private Fileformat.BlobHeader readHeader() throws IOException {
        int headerSize = fileFD.readInt();
        byte[] buf = new byte[headerSize];
        fileFD.readFully(buf);

        return Fileformat.BlobHeader.parseFrom(buf);
    }

    private byte[] readRawBlob(Fileformat.BlobHeader blobHeader) throws IOException {
        byte[] rawBlob = new byte[blobHeader.getDatasize()];

        fileFD.readFully(rawBlob);
        pos += rawBlob.length;
        // System.out.println(pos);
        return rawBlob;
    }


    private void seekNextFileBlockStart(long startOffset) throws IOException {
        fileFD.seek(startOffset);
        // int bufferSize = conf.getInt("io.file.buffer.size", DEFAULT_BUFFER_SIZE);

        final byte[] signature = "OSMData".getBytes();
        int sigPos = 0;
        boolean sigFound = false;

        // searching the OSMData
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];

        do {
            long length = fileFD.read(buffer);
            for (int bufPos = 0; bufPos < length && !sigFound; bufPos++, pos++) {
                if (signature[sigPos] == buffer[bufPos])
                    sigPos++;
                else
                    sigPos = 0;

                if (sigPos == signature.length) {
                    sigFound = true;
                    pos -= signature.length + 6;
                }
            }
        } while (fileFD.available() > 0 && !sigFound);

        // System.err.printf("Starting at %d, Found a blob at position %d\n", startOffset, pos);
        fileFD.seek(pos);
    }

    public boolean nextKeyValue() throws IOException {
        iCurrentEntity++;

        while ((currentEntityList == null || iCurrentEntity >= currentEntityList.size())) {
            try {
                pos++;
                seekNextFileBlockStart(pos);
                if (pos > end) {
                    return false;
                }
                Fileformat.BlobHeader blobHeader = readHeader();
                byte[] blobData = readRawBlob(blobHeader);
                RawBlob currentBlob = new RawBlob(blobHeader.getType(), blobData);
                PbfBlobDecoder blobDecoder = new PbfBlobDecoder(currentBlob, this);
                blobDecoder.run();
                iCurrentEntity = 0;
            } catch (EOFException ex) {
                break;
            }
        }
        return iCurrentEntity < currentEntityList.size();
    }

    public LongWritable getCurrentKey() {
        return new LongWritable(pos);
    }

    public Entity getCurrentValue() {
        return currentEntityList.get(iCurrentEntity).getEntity();
    }

    public float getProgress() {
        return ((float) pos - start) / ((float) end - start);
        // return 0;
    }

    public void close() throws IOException {
        fileFD.close();
    }

    public void complete(List<EntityContainer> decodedEntities) {
        this.currentEntityList = decodedEntities;
    }

    public void error() {
        throw new RuntimeException("Error");
    }
}
