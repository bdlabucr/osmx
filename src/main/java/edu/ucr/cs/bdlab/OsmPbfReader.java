package edu.ucr.cs.bdlab;

//import edu.ucr.cs.bdlab.beast.common.BeastOptions;
//import edu.ucr.cs.bdlab.beast.io.SpatialReader;
//import edu.ucr.cs.bdlab.beast.io.SpatialWriter;
//import org.apache.hadoop.io.LongWritable;
//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.MapFunction;
//import org.apache.spark.sql.*;
//import org.apache.spark.sql.api.java.UDF1;
//import org.apache.spark.sql.types.DataTypes;
//import org.locationtech.jts.geom.Geometry;
//import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
//import org.openstreetmap.osmosis.core.domain.v0_6.Node;
//import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
//import org.openstreetmap.osmosis.core.domain.v0_6.Way;
//import scala.collection.immutable.Map;
//
///**
// * Java codes for osm-extractor
// * Read the pbf file and do Spark Dataframe operation
// */
//class OsmPbfReader {
//    private static JavaRDD<Entity> osmEntities;
//
//    /**
//     * Parsing pbf file using PbfEntityInputFormat.class
//     *
//     * @param pbfFile the input pbf file name
//     */
//    protected OsmPbfReader(String pbfFile) {
//        JavaSparkContext sc = new JavaSparkContext(getSparkConf());
//
//        System.out.println("---------------------------->pbfFile: " + pbfFile);
//        JavaPairRDD<LongWritable, Entity> pairs = sc.newAPIHadoopFile(pbfFile,
//                PbfEntityInputFormat.class, LongWritable.class, Entity.class, sc.hadoopConfiguration());
//
//        osmEntities = pairs.values();
//    }
//
//    private SparkConf getSparkConf() {
//        SparkConf sparkConf = new SparkConf();
//        sparkConf.setAppName("OSM-Extractor");
//        if (!sparkConf.contains("spark.master"))
//            sparkConf.setMaster("local[*]");
//        return sparkConf;
//    }
//
//    /**
//     * Get SparkSession instance and register User-Defined Functions
//     *
//     * @return SparkSession
//     */
//    protected SparkSession getSparkSession() {
//        SparkSession spark = SparkSession
//                .builder()
//                .config(getSparkConf())
//                .appName("OSM-Extractor")
//                .getOrCreate();
//
//        // Set spark log level
//        spark.sparkContext().setLogLevel("WARN");
//
//        // Create UDF to flatten longitude and latitude for SparkSQL functions in Beast
//        spark.sqlContext()
//                .udf()
//                .register("mergePos", (Double lng, Double lat) -> {
//                    double[] res = new double[2];
//                    res[0] = lng;
//                    res[1] = lat;
//                    return res;
//                }, DataTypes.createArrayType(DataTypes.DoubleType));
//
//        // Create UDF to convert geometry to WKT format
//        spark.sqlContext()
//                .udf()
//                .register("covGeom", (UDF1<Geometry, Object>) Geometry::toText, DataTypes.StringType);
//
//        // Create UDF to convert tags (Map format) to String format
//        spark.sqlContext()
//                .udf()
//                .register("covTags",
//                        (Map tagsMap) -> tagsMap.mkString("[", ",", "]"), DataTypes.StringType);
//
//        return spark;
//    }
//
//    /**
//     * Filter pbf entities and return all nodes Dataframe
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> getNodeDf() {
//        SparkSession spark = getSparkSession();
//
//        // Filter Node DataFrame
//        JavaRDD<NodeDf> osmNodes = osmEntities
//                .filter(e -> e instanceof Node)
//                .map(e -> (Node) e)
//                .map(e -> new NodeDf(e.getId(), e.getVersion(), e.getTimestamp().getTime(), e.getChangesetId(),
//                        e.getUser().getId(), e.getUser().getName(), e.getLatitude(), e.getLongitude(), e.getTags()));
//
//        return spark.createDataFrame(osmNodes, NodeDf.class).alias("nodeDf");
//    }
//
//    /**
//     * Filter pbf entities and return all ways Dataframe
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> getWayDf() {
//        SparkSession spark = getSparkSession();
//
//        // Filter Way DataFrame
//        JavaRDD<WayDf> osmWays = osmEntities
//                .filter(e -> e instanceof Way)
//                .map(e -> (Way) e)
//                .map(e -> new WayDf(e.getId(), e.getVersion(), e.getTimestamp().getTime(), e.getChangesetId(),
//                        e.getUser().getId(), e.getUser().getName(), e.getWayNodes(), e.getTags()));
//
//        return spark.createDataFrame(osmWays, WayDf.class).alias("wayDf");
//    }
//
//    /**
//     * Filter pbf entities and return all relations Dataframe
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> getRelationDf() {
//        SparkSession spark = getSparkSession();
//
//        // Filter Relation DataFrame
//        JavaRDD<RelationDf> osmRelations = osmEntities
//                .filter(e -> e instanceof Relation)
//                .map(e -> (Relation) e)
//                .map(e -> new RelationDf(e.getId(), e.getVersion(), e.getTimestamp().getTime(), e.getChangesetId(),
//                        e.getUser().getId(), e.getUser().getName(), e.getMembers(), e.getTags()));
//
//        return spark.createDataFrame(osmRelations, RelationDf.class).alias("relationDf");
//    }
//
//    /**
//     * Select Dangled Nodes by join result's way Id is Null
//     */
//    protected void createNodesTempView() {
//        Dataset<Row> nodeDf = getNodeDf();
//
//        nodeDf.createOrReplaceTempView("nodes");
//    }
//
//    /**
//     * Right outer join way Dataframe and node Dataframe
//     * for getting all ways' coordinates and selecting dangled nodes
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> joinWayNode() {
//        Dataset<Row> nodeDf = getNodeDf();
//
//        nodeDf = nodeDf.
//                withColumn("location", functions.callUDF("mergePos", nodeDf.col("longitude"),
//                        nodeDf.col("latitude")));
//
//        Dataset<Row> wayDf = getWayDf();
//
//        wayDf = wayDf
//                .select(wayDf.col("id"),
//                        wayDf.col("version"),
//                        wayDf.col("timestamp"),
//                        wayDf.col("changeSetId"),
//                        wayDf.col("uid"),
//                        wayDf.col("uname"),
//                        wayDf.col("tagsMap"),
//                        // flatten nodeIds array into multiple rows for following join with node Dataframe
//                        functions.posexplode(wayDf.col("nodeIds")))
//                .withColumnRenamed("col", "nodeId");
//
//        return wayDf
//                .join(nodeDf, nodeDf.col("id").equalTo(wayDf.col("nodeId")), "rightouter");
//    }
//
//    /**
//     * Select Dangled Nodes by join result's way Id is Null
//     */
//    protected void createDangledNodesTempView() {
//        Dataset<Row> joinDf = joinWayNode();
//
//        joinDf = joinDf.
//                filter(joinDf.col("wayDf.id").isNull())
//                .select(joinDf.col("nodeDf.id").alias("id"),
//                        joinDf.col("nodeDf.version").alias("version"),
//                        joinDf.col("nodeDf.timestamp").alias("timestamp"),
//                        joinDf.col("nodeDf.changeSetId").alias("changeSetId"),
//                        joinDf.col("nodeDf.uid").alias("uid"),
//                        joinDf.col("nodeDf.uname").alias("uname"),
//                        joinDf.col("longitude"),
//                        joinDf.col("latitude"),
//                        joinDf.col("nodeDf.tagsMap").alias("tagsMap"));
//
//        joinDf.createOrReplaceTempView("dangledNodes");
//    }
//
//    /**
//     * Select all ways by join result's way Id is NOT Null
//     * Prepare for following SparkSQL function ST_CreateLinePolygon
//     */
//    protected void createWayTempView() {
//        Dataset<Row> wayDf = joinWayNode();
//
//        wayDf = wayDf
//                .filter(wayDf.col("wayDf.id").isNotNull())
//                .orderBy("wayDf.id", "pos")
//                .groupBy("wayDf.id")
//                .agg(functions.first("wayDf.version").alias("version"),
//                        functions.first("wayDf.timestamp").alias("timestamp"),
//                        functions.first("wayDf.changeSetId").alias("changeSetId"),
//                        functions.first("wayDf.uid").alias("uid"),
//                        functions.first("wayDf.uname").alias("uname"),
//                        functions.first("wayDf.tagsMap").alias("tagsMap"),
//                        functions.first("nodeId").alias("first_nodeId"),
//                        functions.last("nodeId").alias("last_nodeId"),
//                        functions.collect_list("nodeId").alias("nodeIds"),
//                        functions.flatten(functions.collect_list("location")).alias("coordinates"));
//
//        wayDf.createOrReplaceTempView("ways");
//    }
//
//    /**
//     * Right outer join relation Dataframe and way Dataframe
//     * for getting all relations' geometries and selecting dangled ways
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> joinRelationWay() {
//        Dataset<Row> wayDf = OsmExtractor.getWayDf();
//
//        Dataset<Row> relationDf = getRelationDf();
//
//        relationDf = relationDf.
//                withColumn("arrays_zip", functions.explode(functions.arrays_zip(relationDf.col("memberIds"),
//                        relationDf.col("entityTypes"),
//                        relationDf.col("memberRoles"))))
//                .select("id", "version", "timestamp", "changeSetId", "uid", "uname", "tagsMap",
//                        "arrays_zip.memberIds", "arrays_zip.entityTypes")
//                .filter(functions.col("entityTypes").equalTo("Way"))
//                .withColumnRenamed("memberIds", "wayId4join");
//
//        return relationDf
//                .join(wayDf, relationDf.col("wayId4join").equalTo(wayDf.col("wayId")), "rightouter");
//    }
//
//    /**
//     * Select Dangled Ways by join result's relation Id is Null
//     */
//    protected Dataset<Row> getDangledWays() {
//        Dataset<Row> joinDf = joinRelationWay();
//
//        return joinDf.
//                filter(joinDf.col("id").isNull())
//                .select(joinDf.col("wayId").alias("id"),
//                        joinDf.col("wayVersion").alias("version"),
//                        joinDf.col("wayTimestamp").alias("timestamp"),
//                        joinDf.col("wayChangeSetId").alias("changeSetId"),
//                        joinDf.col("wayUid").alias("uid"),
//                        joinDf.col("wayUname").alias("uname"),
//                        joinDf.col("wayTagsMap").alias("tagsMap"),
//                        joinDf.col("geometry"));
//    }
//
//    /**
//     * Select all relations by join result's relation Id is NOT Null
//     * Prepare for following SparkSQL function ST_Connect
//     */
//    protected void createRelationTempView() {
//        Dataset<Row> relationDf = joinRelationWay();
//
//        relationDf = relationDf
//                .filter(relationDf.col("id").isNotNull())
//                .groupBy("relationDf.id")
//                .agg(functions.first("relationDf.version").alias("version"),
//                        functions.first("relationDf.timestamp").alias("timestamp"),
//                        functions.first("relationDf.changeSetId").alias("changeSetId"),
//                        functions.first("relationDf.uid").alias("uid"),
//                        functions.first("relationDf.uname").alias("uname"),
//                        functions.first("relationDf.tagsMap").alias("tagsMap"),
//                        functions.collect_list("first_nodeId").alias("first_nodeIds"),
//                        functions.collect_list("last_nodeId").alias("last_nodeIds"),
//                        functions.collect_list("geometry").alias("geometry"));
//
//        relationDf.createOrReplaceTempView("relations");
//    }
//
//    /**
//     * Get all Dangled Nodes, Dangled Ways and Relations for following dataset splitting
//     *
//     * @return Dataset
//     */
//    protected Dataset<Row> getAllObjects() {
//        Dataset<Row> node = OsmExtractor.getNodeDf("dangledNodes");
//        Dataset<Row> way = getDangledWays();
//        Dataset<Row> relation = OsmExtractor.getRelationDf();
//
//        // Store the full dataset in MEMORY_AND_DISK level for reusing
//        return node.union(way).union(relation);
//    }
//
//    /**
//     * Save Dataframe to .csv.bz2 file
//     *
//     * @param df        dataframe
//     * @param directory output directory
//     */
//    protected void dfToCsv(Dataset<Row> df, String directory) {
//        df.withColumn("tags", functions.callUDF("covTags", df.col("tagsMap")))
//                .withColumn("geometry", functions.callUDF("covGeom", df.col("geometry")))
//                .select("geometry", "id", "version", "timestamp", "uid", "uname", "tags")
//                .write()
//                .mode("overwrite")
//                .option("header", "true")
//                .option("delimiter", "\t")
//                .option("quote", "")
//                .option("compression", "bzip2")
//                .csv(directory);
//    }
//
//    /**
//     * Save Dataframe to text file in geojson format
//     *
//     * @param df        dataframe
//     * @param directory output directory
//     */
//    protected void dfToJson(Dataset<Row> df, String directory) {
//        df.map((MapFunction<Row, String>) RowConvert::toJson, Encoders.STRING())
//                .write()
//                .mode("overwrite")
//                .text(directory);
//    }
//
//    /**
//     * Save Dataframe to .geojson file
//     *
//     * @param df        dataframe
//     * @param directory output directory
//     */
//    protected void dfToGeoJson(Dataset<Row> df, String directory) {
//        SpatialWriter.saveFeatures(SpatialReader.dataFrameToSpatialRDD(df), "geojson", directory, new BeastOptions());
//    }
//}