/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab;

import com.esotericsoftware.kryo.io.Input;
import org.openstreetmap.osmosis.core.store.StoreReader;

public class KryoStoreReader implements StoreReader {
  private final Input input;

  public KryoStoreReader(Input input) {
    this.input = input;
  }

  @Override
  public boolean readBoolean() {
    return input.readBoolean();
  }

  @Override
  public byte readByte() {
    return input.readByte();
  }

  @Override
  public char readCharacter() {
    return input.readChar();
  }

  @Override
  public int readInteger() {
    return input.readInt();
  }

  @Override
  public long readLong() {
    return input.readLong();
  }

  @Override
  public double readDouble() {
    return input.readDouble();
  }

  @Override
  public String readString() {
    return input.readString();
  }
}
