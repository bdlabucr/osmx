package edu.ucr.cs.bdlab;

import org.openstreetmap.osmosis.core.domain.v0_6.RelationMember;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RelationDf extends EntityDf implements Serializable {
    private final List<Long> memberIds;
    private final List<String> entityTypes;
    private final List<String> memberRoles;

    public RelationDf(long id, int version, long timestamp, long changeSetId, int uid, String uname, List<RelationMember> relationMembers, Collection<Tag> tags) {
        super(id, version, timestamp, changeSetId, uid, uname, tags);
        memberIds = new ArrayList<>();
        entityTypes = new ArrayList<>();
        memberRoles = new ArrayList<>();
        for (RelationMember relationMember : relationMembers) {
            this.memberIds.add(relationMember.getMemberId());
            switch (relationMember.getMemberType()) {
                case Bound:
                    this.entityTypes.add("Bound");
                    break;
                case Node:
                    this.entityTypes.add("Node");
                    break;
                case Way:
                    this.entityTypes.add("Way");
                    break;
                case Relation:
                    this.entityTypes.add("Relation");
                    break;
            }
            this.memberRoles.add(relationMember.getMemberRole());
        }
    }

    public List<Long> getMemberIds() {
        return memberIds;
    }

    public List<String> getEntityTypes() {
        return entityTypes;
    }

    public List<String> getMemberRoles() {
        return memberRoles;
    }
}
